FROM codercom/code-server:latest

USER root

RUN apt-get update && apt-get install -y sudo && rm -rf /var/lib/apt/lists/*

RUN sudo apt-get update && \
    sudo DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends \
    git \
    docker.io \
    docker-compose

RUN sudo apt-get update && \
    sudo DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends \
    python3-pip

RUN pip3 install --no-cache-dir \
    pylint \
    black \
    virtualenv

RUN code-server --install-extension ms-python.python && \
    code-server --install-extension ms-azuretools.vscode-docker && \
    code-server --install-extension k--kato.docomment && \
    code-server --install-extension scheiblingco.code-pypack && \
    code-server --install-extension mjmcloug.vscode-elixir && \
    sudo apt-get clean && sudo rm -rf /var/lib/apt/lists/*

RUN sudo apt update -y && \
    sudo apt install openjdk-17-jdk openjdk-17-jre -y && \
    sudo apt-get clean && sudo rm -rf /var/lib/apt/lists/*

USER coder

WORKDIR /home/coder

RUN mkdir -p /home/coder/.local/share/code-server/UserData/extensions

RUN pip3 install --no-cache-dir \
    argparse \
    pandas \
    numpy \
    scikit-learn 

RUN sudo apt-get update -y
RUN sudo apt-get install make -y

CMD ["code-server", "--port", "8080", "--user-data-dir", "/home/coder/.local/share/code-server/UserData"]

