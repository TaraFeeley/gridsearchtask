import pandas as pd
import numpy as np

# Define the labels
labels = ['Positive', 'Negative']

# Define the features
features = ['Glucose', 'BloodPressure', 'SkinThickness', 'Insulin', 'BMI', 'DiabetesPedigreeFunction', 'Age']

# Define the number of data points
n = 50

# Generate random data
data = {
    'Category': np.random.choice(labels, size=n),
    'Glucose': np.random.randint(50, 200, size=n),
    'BloodPressure': np.random.randint(50, 150, size=n),
    'SkinThickness': np.random.randint(5, 50, size=n),
    'Insulin': np.random.randint(0, 300, size=n),
    'BMI': np.random.uniform(15, 50, size=n),
    'DiabetesPedigreeFunction': np.random.uniform(0, 2, size=n),
    'Age': np.random.randint(20, 80, size=n)
}

# Save the data as a CSV file
df = pd.DataFrame(data, columns=['Category'] + features)
df.to_csv('sample.csv', index=False)
