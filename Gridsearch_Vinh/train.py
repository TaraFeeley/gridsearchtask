import argparse
import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import LabelEncoder

# Define the command line arguments
parser = argparse.ArgumentParser(description='Perform a grid search on a random forest classifier')
parser.add_argument('input_file', type=str, help='Input CSV file')
parser.add_argument('-o', '--output', type=str, help='Output CSV file', default='output.csv')
parser.add_argument('-n', '--n_jobs', type=int, help='Number of jobs to run in parallel', default=-1)
parser.add_argument('-c', '--cv', type=int, help='Number of cross-validation folds', default=3)

# Define the hyperparameters to search over
param_grid = {
    'n_estimators': [50, 100, 200],
    'max_depth': [None, 10, 20],
    'min_samples_split': [2, 5, 10],
    'min_samples_leaf': [1, 2, 4]
}

# Define the main function
def main(args):
    # Load the input CSV file
    data = pd.read_csv(args.input_file)

    # Convert the labels in the first column to integers using LabelEncoder
    le = LabelEncoder()
    data.iloc[:, 0] = le.fit_transform(data.iloc[:, 0])

    # Split the data into X and Y
    X = data.drop('Age', axis=1)
    Y = data['Age']

    # Create the random forest classifier
    rf = RandomForestClassifier()

    # Create the grid search object
    grid = GridSearchCV(rf, param_grid=param_grid, cv=args.cv, n_jobs=args.n_jobs)

    # Fit the grid search object to the data
    grid.fit(X, Y)

    # Print the results
    print(f"Best parameters: {grid.best_params_}")
    print(f"Best score: {grid.best_score_}")

    # Write the output to a CSV file
    results = pd.DataFrame(grid.cv_results_)
    results.to_csv(args.output, index=False)

if __name__ == '__main__':
    args = parser.parse_args()
    main(args)
